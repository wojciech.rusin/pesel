package pesel;

import java.util.Scanner;

public class Pesel {
    public static void main(String[] args) {
//        System.out.println("Podaj pesel:");
//        Scanner scanner = new Scanner(System.in);
//        String pesel = scanner.next();
        String pesel = "92031913334";


        getBirthYear(pesel);
        getBirthMonth(pesel);
        getBirthDay(pesel);
        getSex(pesel);
        getControlNumber(pesel);
    }
    public static void getControlNumber(String pesel){
        int num1 = Integer.parseInt(pesel.substring(0, 1));
        int num2 = Integer.parseInt(pesel.substring(1, 2));
        int num3 = Integer.parseInt(pesel.substring(2, 3));
        int num4 = Integer.parseInt(pesel.substring(3, 4));
        int num5 = Integer.parseInt(pesel.substring(4, 5));
        int num6 = Integer.parseInt(pesel.substring(5, 6));
        int num7 = Integer.parseInt(pesel.substring(6, 7));
        int num8 = Integer.parseInt(pesel.substring(7, 8));
        int num9 = Integer.parseInt(pesel.substring(8, 9));
        int num10 = Integer.parseInt(pesel.substring(9, 10));
        int num11 = Integer.parseInt(pesel.substring(10, 11));

        int result = 1 * num1 +
                    3 * num2 +
                    7 * num3 +
                    9 * num4 +
                    1 * num5 + 3 * num6 + 7 * num7 + 9 * num8 + 1 * num9 + 3 * num10;
        result %= 10;
        result = 10 - result;
        result %= 10;

        if(result == num11){
            System.out.println("Liczba kontrolna poprawna: " + num11);
        } else {
            System.out.println("Liczba kontrolna błędna");
        }
    }
    public static void getSex(String pesel) {
        int number = Integer.parseInt(pesel.substring(6, 10));
        if (number % 2 == 0) {
            System.out.println("Płeć: kobieta");
        } else {
            System.out.println("Płeć: mężczyzna");
        }
    }

    public static void getBirthDay(String pesel) {
        int day = Integer.parseInt(pesel.substring(4, 6));
        System.out.println("Dzień urodzenia: " + day);
    }

    public static void getBirthMonth(String pesel) {
        int month = Integer.parseInt(pesel.substring(2, 4));
        int birthMonth = 0;
        if (month > 80 && month < 93) {
            birthMonth = month - 80;
        } else if (month > 0 && month < 13) {
            birthMonth = month;
        } else if (month > 20 && month < 33) {
            birthMonth = month - 20;
        } else if (month > 40 && month < 53) {
            birthMonth = month - 40;
        } else {
            System.out.println("Miesiąc urodzenia: błędnie wpisany");
        }

        switch (birthMonth) {
            case 1:
                System.out.println("Miesiąc urodzenia: Styczeń");
                break;
            case 2:
                System.out.println("Miesiąc urodzenia: Luty");
                break;
            case 3:
                System.out.println("Miesiąc urodzenia: Marzec");
                break;
            case 4:
                System.out.println("Miesiąc urodzenia: Kwieceiń");
                break;
            case 5:
                System.out.println("Miesiąc urodzenia: Maj");
                break;
            case 6:
                System.out.println("Miesiąc urodzenia: Czerwiec");
                break;
            case 7:
                System.out.println("Miesiąc urodzenia: Lipiec");
                break;
            case 8:
                System.out.println("Miesiąc urodzenia: Sierpień");
                break;
            case 9:
                System.out.println("Miesiąc urodzenia: Wrzesień");
                break;
            case 10:
                System.out.println("Miesiąc urodzenia: Październik");
                break;
            case 11:
                System.out.println("Miesiąc urodzenia: Listopad");
                break;
            case 12:
                System.out.println("Miesiąc urodzenia: Grudzień");
                break;
        }
    }

    public static void getBirthYear(String pesel) {
        int month = Integer.parseInt(pesel.substring(2, 4));
        String year = pesel.substring(0, 2);

        if (month > 80 && month < 93) {
            System.out.println("Rok urodzenia 18" + year);
        } else if (month > 0 && month < 13) {
            System.out.println("Rok urodzenia 19" + year);
        } else if (month > 20 && month < 33) {
            System.out.println("Rok urodzenia 20" + year);
        } else if (month > 40 && month < 53) {
            System.out.println("Rok urodzenia 21" + year);
        }
    }
}
